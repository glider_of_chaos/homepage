import React, { useState } from 'react';
import { MarkdownSection } from '../../src/components/Markdown';
import { giveAwayData } from '../../src/data/debug';
import { Layout } from '../../src/components/Layout';
import { GiveAway, GiveAwayTemplate } from '../../src/lib/GiveAway';
//import { GiveAwayTemplate } from '../../src/lib/GiveAway';

export default function Lottery() {

  const [giveAway, setGiveaway] = useState(new GiveAway(null));
  const [markdown, setMarkdown] = useState('');

  return (
    <Layout title = "Lottery">
      <main>
        <h1 className='title'>Welcome to the lottery page</h1>
      </main>
      <button type='button' id='create_giveaway'  onClick={() => setGiveaway(new GiveAway(giveAwayData))} >
        Create GiveAway
      </button>
      <button type='button' id='generate_game_list' onClick={() => setMarkdown(giveAway.generateGameList())}>
        Generate Game list
      </button>
      <button type='button' id='show_results' onClick={() => setMarkdown(giveAway.reportWinners())}>
        Announce Winners
      </button>
      <p id='debug_output'>debug data</p>
      <MarkdownSection
        markdown={markdown} />
    </Layout>
  );
}
