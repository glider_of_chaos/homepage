import React from 'react';
import { Layout } from '../src/components/Layout';

export default function Home() {
  return (
    <Layout title = "Index">
      <main>
        <h1 className='title'>Welcome to my HomePage!</h1>
      </main>
    </Layout>
  );
}
