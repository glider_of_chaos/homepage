import seedrandom from 'seedrandom';

export class GiveAway {
  maxUserTickets: number;
  maxGameTickets: number;
  totalValidTickets: number;
  seed: string;
  items: Item[];
  users: User[];
  disqualifiedUsers: disqualifiedUser[];

  constructor(giveAway: GiveAwayTemplate|null) {
    this.maxUserTickets = giveAway?.maxUserTickets ?? 0;
    this.maxGameTickets = giveAway?.maxGameTickets ?? 0;
    this.seed = giveAway?.seed ?? '';
    this.items = giveAway?.items ?? [];
    this.users = giveAway?.users ?? [];

    this.totalValidTickets = 0;
    this.users.forEach((user) => {
      let totalTicketCount = 0;
      let maxTicketsForGame = 0;
      Object.entries(user.distribution).forEach(([game, tickets]) => {
        tickets = tickets ?? 0;
        totalTicketCount += tickets;
        maxTicketsForGame =
          tickets > maxTicketsForGame ? tickets : maxTicketsForGame;
      });
      user.ticketsUsed = totalTicketCount;
      user.maxTickets = maxTicketsForGame;
    });
    this.items.forEach((item) => {
      item.ticketBowl = [] as string[];
    });
    this.disqualifiedUsers = [];
    this.allocateTickets();
  }

  generateGameList() {
    let markdown = '---' +
                   '\n' +
                   '### Game List ###' +
                   '\n' +
                   '|:gift:|Game|Steam Link|' +
                   '\n' +
                   '|:--:|--|:--:|';
    this.items.forEach(game => {
      markdown += '\n' +
                  `|${game.letter.toUpperCase()}|${game.name}|[Link](${game.link})|`;
    });
    return markdown;
  }
  
  listDisqualificationReasons(user: User) {
    let reasons = '';
  
    if (user.songProvided === false) {
      reasons += 'Song is not provided';
    }
    if (user.requirementsMet === false) {
      reasons += '</br>' +
                 'Timeline or badges requirements not met';
    }
    if (user.ticketsUsed && user.ticketsUsed > this.maxUserTickets) {
      reasons += '</br>' +
                 `Total maximum tickets amount exceeded ( ${user.ticketsUsed} / ${this.maxUserTickets} )`;
    }
    if (user.maxTickets && user.maxTickets > this.maxGameTickets) {
      reasons += '</br>' +
                 `Maximum tickets amount for one game exceeded ( ${user.maxTickets} / ${this.maxGameTickets} )`;
    }

    return reasons.replace(/^<\/br>/, '');
  }
  
  allocateTickets() {
    let totalValidTickets = 0;
    this.users.forEach(user => {
      const disqualificationReasons = this.listDisqualificationReasons(user);
      if (disqualificationReasons.length) {
        this.disqualifiedUsers.push({
          user: user.name,
          reason: disqualificationReasons,
        });
      } else {
        Object.entries(user.distribution).forEach(([game, tickets]) => {
          const desiredGame = this.items.find(item => item.letter === game);
          if (desiredGame?.ticketBowl && tickets) {
            for (let i = 0; i < tickets; i++) {
              desiredGame.ticketBowl.push(user.name);
            }
          }
        });
        totalValidTickets += user?.ticketsUsed ?? 0;
      }
    });
    this.totalValidTickets = totalValidTickets;
  }

  drawWinners() {
    // Math.seedrandom(this.seed);
    const randNum = seedrandom.alea(this.seed).double();
    this.items.forEach((item) => {
      if (item.ticketBowl?.length) {
        const winningNum = Math.floor(randNum * item.ticketBowl.length);
        item.winner = item.ticketBowl[winningNum];
      }
    });
  }
  
  getWinnersMarkdown() {
    let markdown = '---' +
                   '\n' +
                   '### Winners ###' +
                   '\n' +
                   '|:gift:|Winner|Chance|' +
                   '\n' +
                   '|--|--|--:|';
    this.items.forEach((item) => {
      if (item.ticketBowl?.length) {
        const ticketsFromWinner = item.ticketBowl.filter(
          (ticketOwner) => ticketOwner === item.winner
        ).length;
        const prettyChance = (
          (100 * ticketsFromWinner) /
          item.ticketBowl.length
        ).toFixed(2);
        markdown += '\n';
        markdown += `|${item.name}|${item.winner}|${prettyChance}%|`;
      } else {
        markdown += '\n';
        markdown += `|${item.name}| - | - |`;
      }
    });
    return markdown;
  }
  
  getPopularityStatsMarkdown() {
    let markdown = '---' +
                   '\n' +
                   '### Popularity ###' +
                   '\n' +
                   `There were ${this.users.length} entries with total number of ${this.totalValidTickets} valid tickets.` +
                   '\n' +
                   '|:gift:|:ticket:|Popularity|' +
                   '\n' +
                   '|--|--|--:|';
    
    this.items.forEach((item) => {
      const gameTickets = item.ticketBowl?.length ?? 0;
      const prettyPercentage = (
        (100 * gameTickets) /
        this.totalValidTickets
      ).toFixed(2);
      markdown += '\n' +
                  `|${item.name}|${gameTickets}|${prettyPercentage}|`;
    });
    
    return markdown;
  }
  
  getDisqualificationMarkdown() {
    let markdown = '---' +
                   '\n' +
                   '### Disqualifications ###' +
                   '\n';
    
    if (this.disqualifiedUsers.length) {
      markdown += '|:(|Reason|' +
                  '\n' +
                  '|--|--|';
      this.disqualifiedUsers.forEach((user) => {
        markdown += '\n' +
                    `|${user.user}| ${user.reason}|`;
      });
    } else {
      markdown += 'None! :blush:';
    }
    return markdown;
  }

  reportWinners() {
    this.drawWinners();
    const markdown = this.getWinnersMarkdown() +
                  '\n\n' +
                  this.getPopularityStatsMarkdown() +
                  '\n\n' +
                  this.getDisqualificationMarkdown();
    return markdown;
  }
}

enum EntryLetter {
  a = 'a',
  b = 'b',
  c = 'c',
  d = 'd',
  e = 'e',
  f = 'f',
  g = 'g',
  h = 'h',
  i = 'i',
  j = 'j',
  k = 'k',
  l = 'l',
}

type TicketForLetter = {
  [key in EntryLetter]?: number;
};

interface User {
  name: string;
  songProvided: boolean;
  requirementsMet: boolean;
  distribution: TicketForLetter;
  ticketsUsed?: number;
  maxTickets?: number;
}

interface Item {
  letter: string;
  name: string;
  link: string;
  ticketBowl?: string[];
  winner?: string;
}

export interface GiveAwayTemplate {
  maxUserTickets: number;
  maxGameTickets: number;
  seed: string;
  items: Item[];
  users: User[];
}

interface disqualifiedUser {
  user: string;
  reason: string;
}