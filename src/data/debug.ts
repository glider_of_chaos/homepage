import { GiveAwayTemplate } from '../lib/GiveAway';

export const giveAwayData: GiveAwayTemplate = {
  maxUserTickets: 20,
  maxGameTickets: 12,
  seed: 'random seed',
  items: [{
    letter: 'a',
    name: 'ENSLAVED: Odyssey to the West Premium Edition',
    link: 'https://store.steampowered.com/app/245280/ENSLAVED_Odyssey_to_the_West_Premium_Edition/'
  }, {
    letter: 'b',
    name: 'Pac-Man 256',
    link: 'https://store.steampowered.com/app/455400/PACMAN_256/'
  }, {
    letter: 'c',
    name: 'GoNNER',
    link: 'https://store.steampowered.com/app/437570/GoNNER/'
  }, {
    letter: 'd',
    name: 'Tekken',
    link: 'https://store.steampowered.com/app/389730/TEKKEN_7/'
  }, {
    letter: 'e',
    name: 'Scribblenauts Unlimited',
    link: 'https://store.steampowered.com/app/218680/Scribblenauts_Unlimited/'
  }, {
    letter: 'f',
    name: 'Dust: An Elysian Tail',
    link: 'https://store.steampowered.com/app/236090/Dust_An_Elysian_Tail/'
  }, {
    letter: 'g',
    name: 'One Way Heroics',
    link: 'https://store.steampowered.com/app/266210/one_way_heroics/'
  }, {
    letter: 'h',
    name: 'Mercenary Kings',
    link: 'https://store.steampowered.com/app/218820/Mercenary_Kings_Reloaded_Edition/'
  }, {
    letter: 'j',
    name: 'Abyss Odyssey',
    link: 'https://store.steampowered.com/app/255070/Abyss_Odyssey/'
  }, {
    letter: 'k',
    name: 'Vanguard Princess + Lilith Pack + Hilda Rize Pack',
    link: 'https://store.steampowered.com/app/262150/Vanguard_Princess/'
  }, {
    letter: 'l',
    name: 'REVOLVER 360 RE:ACTOR',
    link: 'https://store.steampowered.com/app/313400/REVOLVER360_REACTOR/'
  }],
  users:[{
    name:'Timmy',
    songProvided: true,
    requirementsMet: true,
    distribution: {
      b: 5,
      c: 5,
      d: 5}
  },{
    name:'Billy',
    songProvided: true,
    requirementsMet: true,
    distribution: {
      b: 12,
      f: 3}
  },{
    name:'Spike',
    songProvided: true,
    requirementsMet: true,
    distribution: {
      a: 1,
      c: 9,
      d: 5}
  },{
    name:'Dolf',
    songProvided: true,
    requirementsMet: true,
    distribution: {
      a: 12,
      c: 9,
      d: 5}
  },{
    name:'Tony',
    songProvided: true,
    requirementsMet: true,
    distribution: {
      a: 2,
      b: 1,
      c: 16,
      d: 1}
  },{
    name:'Larry',
    songProvided: true,
    requirementsMet: false,
    distribution: {
      a: 2,
      b: 2,
      c: 6,
      d: 5}
  },{
    name:'Alfred',
    songProvided: false,
    requirementsMet: true,
    distribution: {
      a: 1,
      c: 8,
      d: 6}
}]};