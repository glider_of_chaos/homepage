import MarkdownIt from 'markdown-it';

export const MarkdownSection = (props: { markdown: string }) => {

  return (
    <div className='container'>
      <pre>
        {props.markdown}
      </pre>
      <div
        className='item'
        id='markdown_render'
        dangerouslySetInnerHTML={{
          __html: (new MarkdownIt()).render(props.markdown)
        }}
      />
    </div>
  );
}