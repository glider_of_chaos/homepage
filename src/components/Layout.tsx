import React from 'react';
import Head from 'next/head';

export const Layout = (props: { title: string, children: React.ReactNode }) => {
  return (
    <div className='container'>
      <Head>
        <title>{props.title}</title>
        <link rel='icon' href='/favicon.ico' />
      </Head>

      <Header />

      {props.children}

      <Footer />
    </div>
  );
}

const Header = () => {
  return (
    <div>
      <p>Header Placeholder Text v2</p>
    </div>
  );
}

const Footer = () => {
  return (
    <div>
      <p>Footer Placeholder Text v2</p>
    </div>
  );
}
